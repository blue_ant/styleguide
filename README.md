# Предварительный styleguide

### Именования файлов

Если добавляете в репозиторий файлы картинок, видео, json, pug или svg - то называйте файлы в стиле [snake_case](https://ru.wikipedia.org/wiki/Snake_case)

**Например:** 

+ acrobat_reader_logo.svg
+ developer_page_gallery.json
+ compass_white.png
+ page_bg_pattern.jpg
+ news_page.pug
+ contacts_page.html

### Именование css классов

В проектах используем [BEM](https://ru.bem.info/methodology/)-подобную css конвенцию именования классов.
Просьба следовать такому порядку именования:

    .Block
    .Block_element
    .Block_element-mod

    .AnotherBlockName
    .AnotherBlockName_elementName
    .AnotherBlockName_elementName-longModificatorName
    
т.е. название блока с заглавной буквы, название элемента отделаяется подчеркиванием, название модификатора отделяется дефисом. 